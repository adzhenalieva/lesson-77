import React, { Component, Fragment } from 'react';
import './App.css';
import {Container} from "reactstrap";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Posts from "./containers/Posts/Posts";
import {Switch, Route} from 'react-router-dom';
import NewPost from "./containers/NewPost/NewPost";

class App extends Component {
  render() {
    return (
        <Fragment>
          <header><Toolbar/></header>
          <Container style={{marginTop: '20px', textAlign: "center"}}>
            <Switch>
              <Route path="/" exact component={Posts}/>
              <Route path="/posts/new" exact component={NewPost}/>
            </Switch>
          </Container>
        </Fragment>
    );
  }
}

export default App;
