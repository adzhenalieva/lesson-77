import axios from "../axios-api";

export const FETCH_POSTS_SUCCESS = 'FETCH_POSTS_SUCCESS';
export const FETCH_POSTS_FAILURE = 'FETCH_POSTS_FAILURE';
export const CREATE_POST_SUCCESS = "CREATE_POST_SUCCESS";

export const fetchPostSuccess = posts => {
    return {type: FETCH_POSTS_SUCCESS, posts};
};
export const createPostSuccess = () => ({type: CREATE_POST_SUCCESS});
export const dataFailure = error => ({type: FETCH_POSTS_FAILURE, error});

export const fetchPosts = () => {
    return dispatch => {
        return axios.get('/posts').then(
            response => {
                dispatch(fetchPostSuccess(response.data))
            },
            error => {
                dispatch(dataFailure(error));
            }
        );
    };
};

export const createPosts = postData => {
    return dispatch => {
        return axios.post('/posts', postData).then(
            () => {
                dispatch(createPostSuccess())
            }, error => {
                dispatch(dataFailure(error.response.data.message));
            }
        );
    };
};
