import React, {Component, Fragment} from 'react';
import {Button, Card, CardBody, CardTitle, CardText, CardColumns} from "reactstrap";
import {fetchPosts} from "../../store/actions";
import {connect} from "react-redux";
import PostThumbnail from "../../components/PostThumbnail/PostThumbnail";
import {Link} from "react-router-dom";


class Posts extends Component {

    componentDidMount() {
        this.props.onFetchPosts();
    }

    render() {
        let posts = this.props.posts;
        if (posts.length === 0) {
            posts = <h2>Add new post</h2>;
        } else {
            posts = this.props.posts.map(post => (
                <Card body inverse color="warning" key={post.id} style={{marginBottom: '10px'}}>
                    <CardBody>
                        <CardTitle className="mb-4">
                            <strong>{post.author}</strong>
                        </CardTitle>
                        <PostThumbnail image={post.image}/>
                        <CardText>
                            {post.message}
                        </CardText>
                    </CardBody>
                </Card>
            ));
        }
        return (
            <Fragment>
                <Link to='/posts/new'>
                    <Button color="warning"
                            className="float-right"
                    >
                        Add post
                    </Button>
                </Link>
                <h1>
                    Posts
                </h1>
                <CardColumns>
                    {posts}
                </CardColumns>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        posts: state.posts
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchPosts: () => dispatch(fetchPosts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Posts);