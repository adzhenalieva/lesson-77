import React, {Component, Fragment} from 'react';
import PostForm from "../../components/PostForm/PostForm";
import {connect} from "react-redux";
import {createPosts} from "../../store/actions";

class NewPost extends Component {

    createProduct = postData => {
        this.props.onPostCreated(postData).then(()=> {
            this.props.history.push('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h2>New post</h2>
                <PostForm onSubmit={this.createProduct}/>
            </Fragment>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    onPostCreated: postData => dispatch(createPosts(postData))

});
export default connect(null, mapDispatchToProps)(NewPost);