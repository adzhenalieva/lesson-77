const express = require('express');
const posts = require('./app/posts');
const cors = require('cors');
const fileDb = require('./fileDb');
const app = express();
fileDb.init();


const port = 8000;
app.use(express.json());
app.use(express.static('public'));
app.use(cors());
app.use('/posts', posts);

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});