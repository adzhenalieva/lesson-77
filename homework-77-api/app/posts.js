const express = require('express');
const multer = require('multer');
const path = require('path');
const fileDb = require('../fileDb');
const config = require('../config');
const nanoid = require('nanoid');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    res.send(fileDb.getItems());
});

router.post('/', upload.single('image'), (req, res) => {
    const post = req.body;
    if(post.message  === ""){
        res.status(400).send({message: "Error 400"});
    }
    if(req.file){
        post.image = req.file.filename;
    }
    if(post.author === ""){
        post.author = "Anonymous";
    }
    post.id = nanoid();
    fileDb.addItem(post);
    res.send({message: "Ok"});
});

module.exports = router;